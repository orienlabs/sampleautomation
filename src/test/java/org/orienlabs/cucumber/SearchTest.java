package org.orienlabs.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.Test;

@CucumberOptions(
        monochrome = true,
        features = "src/test/resources/features/sample/Search.feature",
        plugin = {"json:target/cucumber-report.json", "com.cucumber.listener.ExtentCucumberFormatter:output/report.html"},
        format = { "pretty","html: cucumber-html-reports", "json: cucumber-html-reports/cucumber.json" },
        dryRun = false, strict= true,
        glue = {"org.orienlabs.orienworkx.cucumber"})
@Test
public class SearchTest extends AbstractTestNGCucumberTests {
}