package org.orienlabs.keyword;

import java.io.File;

import org.orienlabs.orienworkx.keyword.KeywordDriver;
import org.orienlabs.orienworkx.robot.MobileApps;
import org.orienlabs.orienworkx.robot.MobileDevices;
import org.testng.annotations.Test;

@SuppressWarnings("unused")
@Test
public class SearchTest {
	
	@Test
	public void searchDefault() throws Throwable
	{
		/*
		UIDriver.startBrowser();
		UIDriver.navigate("https://www.google.co.in");
		UIElement.byName("q").setText("test mode");
		UIElement.byName("q").submit();
		UIDriver.takePngScreenshot();
		UIDriver.takeHtmlScreenshot();
		*/
		
		KeywordDriver.runTestCase(new File("src/test/resources/keywords/search.default.01.yml"));
		
		//MobileApps.getApp("", "");
		//MobileDevices.getDevice("");
	}
}