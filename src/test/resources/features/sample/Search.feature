Feature: BDD Search

  Scenario: Search for a basic keyword
    Given I have launched browser
    And I navigate to 'http://www.google.com'
    And I type 'Test Anything' in textbox > name=q
    And I submit form > name=q